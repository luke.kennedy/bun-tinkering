-- Apparently all migrations files *MUST* begin with exactly 14 digits followed by an underscore, then alphanumerics, underscores and dashes ending in .up.sql
-- Note that if you use those 14 digits as a timestamp, you get accuracy to the second, which is way too precise for a hand-written migration file
CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL
);
