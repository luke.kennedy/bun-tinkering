module gitlab.com/luke.kennedy/bun-tinkering

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	github.com/uptrace/bun v1.0.22
	github.com/uptrace/bun/dialect/pgdialect v1.0.22
	github.com/uptrace/bun/driver/pgdriver v1.0.22
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220126234351-aa10faf2a1f8 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	mellium.im/sasl v0.2.1 // indirect
)
