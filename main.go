package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"github.com/uptrace/bun/migrate"
)

func main() {
	// Connect and wrap
	db := wrap(connect())
	defer db.Close()

	migrations := migrate.NewMigrations(migrate.WithMigrationsDirectory("./migrations"))
	err := migrations.Discover(os.DirFS("."))
	if err != nil {
		log.Printf("Could not discover migrations: %v", err)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	migrator := migrate.NewMigrator(db, migrations)
	err = migrator.Init(ctx)
	if err != nil {
		log.Printf("Could not init migrations: %v", err)
		return
	}
	_, err = migrator.Migrate(ctx)
	if err != nil {
		log.Printf("Could not migrate: %v", err)
		_, err = migrator.Rollback(ctx)
		if err != nil {
			log.Printf("Could not roll back: %v", err)
			return
		}
		log.Printf("Rolled back")
		return
	}
	log.Print("Migrated DB to latest")
}

// Connect at the SQL layer with a Bun-provided PG driver
func connect() *sql.DB {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("could not load environment file: %v", err)
	}
	pwd := os.Getenv("LOCAL_DB_PASSWORD")
	dsn := fmt.Sprintf("postgres://postgres:%s@localhost:5432/bun_test?sslmode=disable", url.PathEscape(pwd))
	db := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	err = db.Ping()
	if err != nil {
		log.Fatalf("could not ping DB: %v", err)
	}
	log.Print("Connected to DB")
	return db
}

// Wrap the *sql.DB as a *bun.DB
func wrap(db *sql.DB) *bun.DB {
	bunDB := bun.NewDB(db, pgdialect.New())
	err := bunDB.Ping()
	if err != nil {
		log.Fatalf("could not ping wrapped DB: %v", err)
	}
	log.Print("Wrapped as Bun DB")
	return bunDB
}
