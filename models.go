package main

import "github.com/uptrace/bun"

// https://bun.uptrace.dev/guide/models.html#struct-tags
type User struct {
	bun.BaseModel `bun:"table:users,alias:u"`

	ID   int64  `bun:"id,pk,autoincrement"`
	Name string `bun:"name,notnull"`
}
